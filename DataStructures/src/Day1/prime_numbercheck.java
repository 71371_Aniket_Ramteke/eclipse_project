package Day1;

public class prime_numbercheck {
	
	static boolean isPrime(int n) {
		if(n == 1) {
			return false;
		}
		for(int i = 2;i*i <= n;i++) {
			if(n%i == 0) {
				return false;
			}
			return true;
		}
		return false;
	}

	
	public static void main(String[] args) {
		System.out.println(prime_numbercheck.isPrime(13));
	}
}
