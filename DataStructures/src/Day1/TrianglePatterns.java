package Day1;

public class TrianglePatterns {
	public static void leftAlignedTriangle(int x) {
		for(int i = 0;i<4;i++) {
			for(int j = 0;j<i+1;j++) {
				System.out.print(x);
			}
			x++;
			System.out.println();
		}
	}
	
	public static void leftAlignedTriangle(String pattern) {
		for(int i = 0;i<4;i++) {
			for(int j = 0;j<i+1;j++) {
				System.out.print(pattern);
			}
			System.out.println();
		}
	}
	
	public static void rightAlignedTriangle(String pattern) {
		for(int i = 0; i<4;i++) {
			for( int j = 0 ;j<4-i;j++) {
				System.out.print(" ");
			}
			for(int j = 0;j<i+1;j++) {
				System.out.print(pattern);
			}
			System.out.println();
			
		}
	}
	
	public static void rightAlignedTriangle(int pattern) {
		for(int i = 0; i<4;i++) {
			for( int j = 0 ;j<4-i;j++) {
				System.out.print(" ");
			}
			for(int j = 0;j<i+1;j++) {
				System.out.print(pattern);
			}
			pattern++;
			System.out.println();
			
		}
	}
	
	
	
	public static void main(String[] args) {
		
		int a = 1;
		for(int i = 0; i<4;i++) {
			for( int j = 0 ;j<4-i;j++) {
				System.out.print(" ");
			}
			for(int j = 0;j<i+1;j++) {
				System.out.print(a);
			}
			a++;
			System.out.println();
			
		}


	
	}
	
	
}
