package Practice;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class HashMapExample {
	public static void main(String[] args) {
		Map<String, Country> CountryMap = new HashMap<>();
		
		Country c1 = new Country();
		Country c2 = new Country("Tokyo","Japan");
		Country c3 = new Country("Berlin","Germany");
		CountryMap.put("IND", c1);
		CountryMap.put("JPN", c2);
		CountryMap.put("GER", c3);
		
		Collection<Country> collectCountry = CountryMap.values();
		for (Country country : collectCountry) {
			System.out.println(country);
		}
	}
}
