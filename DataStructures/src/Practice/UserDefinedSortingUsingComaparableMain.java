package Practice;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

public class UserDefinedSortingUsingComaparableMain {
	public static void main(String[] args) {
		Comparator<Planet> comparison = new SortingAlgo();
		SortedSet<Planet> planetSet = new TreeSet<>(comparison);
		planetSet.add(new Planet("APPP", 5));
		planetSet.add(new Planet("cok", 7));
		planetSet.add(new Planet("baba", 8));
		planetSet.add(new Planet("chika", 15));
		
		for (Planet planet : planetSet) {
			System.out.println(planet);
		}
	}
}
