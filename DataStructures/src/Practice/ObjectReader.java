package Practice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectReader {
	public static void main(String[] args) {
		String path = "./src/Practice/Person.txt";
		String serializattionPath = "./src/Practice/PersonSerialize.txt";
		File file = new File(path);
		File serialFile = new File(serializattionPath);
		try(	
				FileReader fread = new FileReader(file);
				BufferedReader bread = new BufferedReader(fread);
				FileOutputStream fout = new FileOutputStream(serialFile);
				ObjectOutputStream oout = new ObjectOutputStream(fout);
				FileInputStream fin = new FileInputStream(serialFile);
				ObjectInputStream oin = new ObjectInputStream(fin)
				){
			while(true) {
			String personData = bread.readLine();
			if(personData == null) {break;}
			String[] person = personData.split(":");
			Person p = new Person(person[0],person[1],Integer.parseInt(person[2]));
			oout.writeObject(p);
			System.out.println("object written successfully");
			try {
				Object obj = oin.readObject();
				System.out.println((Person)obj);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
			System.out.println("object written successfully");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
	}

}
