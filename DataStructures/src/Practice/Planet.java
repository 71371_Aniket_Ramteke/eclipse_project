package Practice;

import java.util.Objects;

public class Planet implements Comparable<Planet> {
	private String nameOfPlanet;
	private Integer moons;
	public Planet(String nameOfPlanet, Integer moons) {
		this.nameOfPlanet = nameOfPlanet;
		this.moons = moons;
	}
	@Override
	public int hashCode() {
		return Objects.hash(moons, nameOfPlanet);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Planet other = (Planet) obj;
		return Objects.equals(moons, other.moons) && Objects.equals(nameOfPlanet, other.nameOfPlanet);
	}
	public String getNameOfPlanet() {
		return nameOfPlanet;
	}
	public void setNameOfPlanet(String nameOfPlanet) {
		this.nameOfPlanet = nameOfPlanet;
	}
	public Integer getMoons() {
		return moons;
	}
	public void setMoons(Integer moons) {
		this.moons = moons;
	}
	@Override
	public int compareTo(Planet o) {
		Integer moon1 = moons;
		Integer moon2 = o.getMoons();
		
		return moon1.compareTo(moon2);
	}
	@Override
	public String toString() {
		return "Planet [nameOfPlanet=" + nameOfPlanet + ", moons=" + moons + "]";
	}
	
	
}
