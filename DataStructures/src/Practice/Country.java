package Practice;

import java.util.Objects;

public class Country {
	private String capital;
	private String countryName;
	public Country() {
		this.capital = "Delhi";
		this.countryName = "India";
	}
	public Country(String capital, String countryName) {
		this.capital = capital;
		this.countryName = countryName;
	}
	public String getCapital() {
		return capital;
	}
	public void setCapital(String capital) {
		this.capital = capital;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	@Override
	public int hashCode() {
		return Objects.hash(capital, countryName);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		return Objects.equals(capital, other.capital) && Objects.equals(countryName, other.countryName);
	}
	@Override
	public String toString() {
		return "Country [capital=" + capital + ", countryName=" + countryName + "]";
	}
	
	
	
	
	
}
