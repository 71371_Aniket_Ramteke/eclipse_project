package Practice;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class FileReadMain {
	public static void main(String[] args) {
		String filePath = "./src/Practice/java.txt";
		
		try(
				InputStream tFin = new FileInputStream(filePath);
				InputStream bFin = new BufferedInputStream(tFin);
				)
		{
			while(true) {
			int data = bFin.read();
			System.out.print((char)data);
			if(data == -1) {
				break;
			  }
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
