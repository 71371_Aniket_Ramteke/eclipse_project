package Practice;

public class GreetingRunnable implements Runnable {
	
	private String message;
	private int delayTime;
	
	public GreetingRunnable(String message,int dt) {
		this.message = message;
		this.delayTime = dt;
		
	}

	@Override
	public void run() {
		for(int i = 0;i<5;i++) {
			System.out.println(message);
			try {
				Thread.sleep(delayTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
