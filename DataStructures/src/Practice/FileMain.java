package Practice;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileMain {
	public static void main(String[] args) {
		String path = "./src/Practice/java.txt";
		String nexPath = "./src/Practice/a.txt";
		File inputfile = new File(path);
		File outputfile = new File(nexPath);
		boolean availability = inputfile.exists();
		if(availability) {
		try(
				InputStream fin = new FileInputStream(inputfile);
				InputStream bin = new BufferedInputStream(fin);
				OutputStream fout = new FileOutputStream(outputfile);
				OutputStream bout = new BufferedOutputStream(fout)
				){
			byte[] fileData = new byte[(int)inputfile.length()];
			fin.read(fileData);
			String fileKaContents = new String(fileData);
			System.out.println(fileKaContents);
			fout.write(fileData);
			System.out.println("File written successfully!");
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
		
	}

}
