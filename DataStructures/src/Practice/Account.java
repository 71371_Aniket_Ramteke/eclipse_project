package Practice;

public class Account {
	private Float balance;

	public Account(Float balance) {
		this.balance = balance;
	}
	
	public void withdraw(Float amount) throws LowBalanceException {
		if(amount>balance) {
			LowBalanceException ei = new LowBalanceException();
			throw ei;
		}
		balance = balance - amount;
		System.out.println("Updated balance : "+balance);
	}

}
