package Practice;

public class UserDefinedException extends Exception {
	
	public UserDefinedException () {
		
	}
	
	@Override
	public String getMessage() {
		return "Name not found";
	}
}
