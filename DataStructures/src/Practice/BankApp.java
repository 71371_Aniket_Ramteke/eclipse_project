package Practice;

public class BankApp {
	public static void main(String[] args) {
		Account account = new Account(5000f);
		try {
			account.withdraw(6000f);
		} catch (LowBalanceException e) {
			e.printStackTrace();
		}
	}

}
