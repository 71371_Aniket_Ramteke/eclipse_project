package Practice;

public class MessageThreadingMain {
	public static void main(String[] args) {
		MessageThread helloMessage = new MessageThread("thread1");
		MessageThread welcomeMessage = new MessageThread("thread2");
		helloMessage.start();
		welcomeMessage.start();
	}
}
