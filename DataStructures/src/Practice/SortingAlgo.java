package Practice;

import java.util.Comparator;

public class SortingAlgo implements Comparator<Planet> {

	@Override
	public int compare(Planet o1, Planet o2) {
		Integer moon1 = o1.getMoons();
		Integer moon2 = o2.getMoons();
		
		return moon2.compareTo(moon1);
	}

}
