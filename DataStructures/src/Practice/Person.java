package Practice;

import java.io.Serializable;

public class Person implements Serializable{
	private String fname;
	private String lname;
	private Integer age;
	public Person(String fname, String lname, Integer age) {
		this.fname = fname;
		this.lname = lname;
		this.age = age;
	}
	@Override
	public String toString() {
		return "Person [fname=" + fname + ", lname=" + lname + ", age=" + age + "]";
	}
	
}
