package Practice;

import java.util.SortedSet;
import java.util.TreeSet;

public class UserDefinedSortingUsingComaparatorMain {
	public static void main(String[] args) {
		SortedSet<Planet> planetSet = new TreeSet<>();
		planetSet.add(new Planet("APPP", 5));
		planetSet.add(new Planet("cok", 7));
		planetSet.add(new Planet("baba", 8));
		planetSet.add(new Planet("chika", 15));
		
		for (Planet planet : planetSet) {
			System.out.println(planet);
		}
	}
}
