package Day1_Amar_pachal;

import java.util.Scanner;

public class StackExample {
//parts 
	int stack[],tos,maxSize;
	
	void createStack(int size) {
		stack=new int[size];
		tos=-1;
		maxSize=size;
	}
	
	void push(int element) {
		tos++;
		stack[tos]=element;
	}
	
	int pop() {
		int temp=stack[tos];
		tos--;
		return (temp);
	}
	
	int peak() {
		return stack[tos];
	}
	
	boolean is_Empty() {
		if (tos==-1) {
			return true;
		} else {
			return false;
		}
	}
	
	void printStack() {
		for(int i = tos;i>=0;i--)
			System.out.println(stack[i]);
	}

	boolean is_Full() {
		if (tos==maxSize-1) {
			return true;
		} else {
			return false;
		}
	}
	
	public static void main(String[] args) {
		StackExample object = new StackExample();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of the stack!");
		int size = sc.nextInt();
		object.createStack(size);
		int choice;
		do {
			System.out.println("1.Push\n2.Pop\n3.Peek\n4.Print\n0.Exit");
			int userInput = sc.nextInt();
			switch(userInput) {
			
			case 1 :  { if (object.is_Full()!=true) {
				System.out.println("Enter element:");
				int element = sc.nextInt();
				object.push(element);
			} else {
				System.out.println("Stack is full");
			}
			break;
			   }
			
			case 2 : { if (object.is_Empty()!=true) {
				object.pop();
			} else {
				System.out.println("Stack is empty");
			}
			break;
			   }
			
			
			case 3 : {
				if (object.is_Empty()!=true) {
					System.out.println(object.peak()); 
				} else {
					System.out.println("Stack is empty");
				}
				break;
				}
			
			
			case 4 : {
				if (object.is_Empty()!=true) {
					object.printStack();
				} else {
					System.out.println("Stack is empty");
				}
				break;
			}
			
			}
			
			choice = userInput;
			
		}while(choice!=0);
		
		
	}
}
