package DataStructures;

public interface Stack<T> {
	//stack methods
	public int size();
	public boolean isEmpty();
	public T top() throws EmptyStackException ;
	public void push(T t) throws FullStackException;
	public T pop() throws EmptyStackException ;
}
