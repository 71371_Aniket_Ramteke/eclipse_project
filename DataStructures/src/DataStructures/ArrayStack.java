package DataStructures;

public class ArrayStack implements Stack<String> {
	//constructor
	public ArrayStack() {
		array = new String[CAPACITY];
	}

	@Override
	public int size() {
		return top+1;
	}

	@Override
	public boolean isEmpty() {
		return top < 0;			//if -1 then empty, will return true
	}

	@Override
	public String top() throws EmptyStackException {
		if(isEmpty())
			throw new EmptyStackException("Stack is empty.");
		
		return array[top];
	}

	@Override
	public void push(String s) throws FullStackException {
		//throw exception if stack is full
		if(size() == CAPACITY)
			throw new FullStackException("Stack is full");
		//if space in stack then put element at the top of stack
		array[++top]=s;
	}

	@Override
	public String pop() throws EmptyStackException {
		if(isEmpty())
			throw new EmptyStackException("Stack is empty.");
		
		//if not empty remove the string by putting null in its place
		String s = array[top];
		array[top--]=null;
		
		return s;
	}

	//class fields
	private String[] array;
	private int top=-1;
	private final int CAPACITY=1000;
}
