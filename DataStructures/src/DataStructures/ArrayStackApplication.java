package DataStructures;

public class ArrayStackApplication {

	public static void main(String[] args) {
		Stack<String> myStack = new ArrayStack();

		try {
			myStack.push("class");
			myStack.push("interface");
			myStack.push("abstracat");
			myStack.push("boolean");
			myStack.push("static");
			myStack.push("main");
		} catch (FullStackException e) {
			e.printStackTrace();
		}
		
		String topElement;
		try {
			topElement = (String) myStack.top();	//not removing, just retrning string
			System.out.println(topElement);

			topElement = (String) myStack.top();	// main
			System.out.println(topElement);

			topElement = (String) myStack.pop();	// main
			System.out.println(topElement);

			topElement = (String) myStack.pop();	//static
			System.out.println(topElement);
			
			//retrieve all elements in the stack
			System.out.println("Displaying all remaining elements in the stack");
			while(!myStack.isEmpty()) {
				System.out.println(myStack.pop());
			}
		} catch (EmptyStackException e) {
			e.printStackTrace();
		}
		
		
	}
}
